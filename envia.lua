-- INÍCIO DO APP
APP = coroutine.create(
function()
--------------------------------------------------------------------------------
-- RECUPERA DADOS DOS CONTATOS
--------------------------------------------------------------------------------
package.path = package.path .. ';lib/?.lua'

require "ncluasoap" -- Adiciona API NCLuaSOAP
require "controle" -- Adiciona arquivo de controle com os dados dos usuários

  local vazio
  local cod, result
  contatos = lista()

  --------------------------------------------------------------------------------
-- NCLuaSOAP - enviando para o webservice
--------------------------------------------------------------------------------

for i=1,#lista,1 do
  local function getResponse(result)

    print(result)

  end

  local msgTable = {
    -- TODO fazer chamar o webservice localmente baseado no ip
    address = "https://webservicemeavisebot.000webhostapp.com/server.php",
    namespace = "server.codigo",
    operationName = "codigo",
    params = {
      cod = contatos[i].codigo,
      msg = contatos[i].mensagem
          }

 }
  ncluasoap.call(msgTable, getResponse,'1.1')
end


--------------------------------------------------------------------------------
-- FIM DA CHAMADA NUSOAP
--------------------------------------------------------------------------------
    END = true
end


) -- FIM DO APP

event.register(function(evt)
  if (evt.class ~= 'ncl') and (evt.class ~= 'tcp') then return end
  if (evt.type == 'presentation') and (evt.action == 'start') then
    assert(coroutine.resume(APP))
  end
end)
