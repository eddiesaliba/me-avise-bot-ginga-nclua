tmpDados = {}
usuario = {}

local caminho = '/home/nataniel/workspace/SmartHelpTV/'
local novoUsuario = {}
cont = 0
-- recupera os contatos no arquivo e armazena em uma tabela temporária
function Contato(c)  tmpDados[c] = true cont = cont + 1  end

-- chama o arquivo de dados
dofile(caminho..'contatos.lua')

function inserir(novoUsuario)
  local atualizaUsuarios = {}
  for usuario in pairs(tmpDados) do
    table.insert(atualizaUsuarios, usuario)
  end
  table.insert(atualizaUsuarios, novoUsuario)
  atualizar(atualizaUsuarios)
end

--[[ Futuras implementações em NCL (necessário programar o formulário em NCL)

function atualizar(novaTabela)
  file, err = io.open(caminho..'contatos.lua','w+')
  for i,usuario in pairs(novaTabela) do
    --print(usuario.nome, usuario.codigo, usuario.mensagem)
    file:write("\n","Contato{nome = '" .. usuario.nome .."',codigo = '"..usuario.codigo.."',mensagem = '"..usuario.mensagem.."'}")
  end
  file:close()
end

function alterar(novoUsuario, codigo)
  local atualizaUsuarios = {}

  for usuario in pairs(tmpDados) do
    if usuario.codigo == codigo then
        usuario = novoUsuario
    end
    table.insert(atualizaUsuarios,usuario)
  endPP
  atualizar(atualizaUsuarios)
end

function deletar(codigo)
  local atualizaUsuarios = {}

  for usuario in pairs(tmpDados) do
    if usuario.codigo ~= codigo then
        table.insert(atualizaUsuarios,usuario)
    end
  end
  atualizar(atualizaUsuarios)
end

--]]

function listaCodigos()
  codigos = {}

  for usuario in pairs(tmpDados) do
    table.insert(codigos, usuario.codigo)
  end

  return codigos
end

function lista()
  lista = {}
  for usuario in pairs(tmpDados) do
  --  if usuario.codigo == "989898" then
      table.insert(lista, usuario)
      print(usuario.nome, usuario.codigo, usuario.mensagem)
  --  end
  end
  return lista
end

function setNome(nome)
  usuario.nome = nome
end

function setCodigo(cod)
  usuario.codigo = cod
end

function setMensagem(msg)
  usuario.mensagem = msg
end

function quebraTexto(entrada, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={} ; i=1
        for str in string.gmatch(entrada, "([^"..sep.."]+)") do
                t[i] = str
                i = i + 1
        end
        return t
end
